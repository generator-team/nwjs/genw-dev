# genw-dev

An NW.js based desktop application project whose primary focus is to test all the basic features, methods, properties of NW.js so that the tested functionalities could be directly included in the final project. A test project for the final Generator.

![LaTeX Layout](./src/assets/images/Latex_layout.svg)

## Context menu (empty)

```javascript
var contextMenu = new nw.Menu();
```

> Add menu items

```javascript
contextMenu.append(new nw.MenuItem({
  label: 'Sort by',
  click: function(){
    alert('You are trying to sort');
  }
}));

contextMenu.append(new nw.MenuItem({
  label: 'Refresh',
  click: function(){
    // Code to refresh
    alert('You attemted refreshing the system');
  }
}));
```

> Finally 

```javascript
// Hooks the "contextmenu" event
document.body.addEventListener('contextmenu', function(ev) {
  // Prevent showing default context menu
  ev.preventDefault();
  // Popup the native context menu at place you click
  menu.popup(ev.x, ev.y);

  return false;
}, false);
```

## Application menu

```javascript
var appMenu = new nw.Menu({type: "menubar"});
```

> Add menu items

```javascript
var fileSubMenu = new nw.Menu();
var editSubMenu = new nw.Menu();

fileSubMenu.append(new nw.MenuItem({label: "Open File"}));
fileSubMenu.append(new nw.MenuItem({label: "Open Folder"}));
fileSubMenu.append(new nw.MenuItem({type: "separator"}));
fileSubMenu.append(new nw.MenuItem({label: "Exit"}));

editSubMenu.append(new nw.MenuItem({label: "Undo"}));
editSubMenu.append(new nw.MenuItem({label: "Redo"}));
editSubMenu.append(new nw.MenuItem({type: "separator"}));
editSubMenu.append(new nw.MenuItem({label: "Cut"}));
editSubMenu.append(new nw.MenuItem({label: "Copy"}));
editSubMenu.append(new nw.MenuItem({label: "Paste"}));
editSubMenu.append(new nw.MenuItem({label: "Select all"}));

appMenu.append(new nw.MenuItem({
  label: "File",
  submenu: fileSubMenu
}));

appMenu.append(new nw.MenuItem({
  label: "Edit",
  submenu: editSubMenu
}));
```

> Finally

```javascript
nw.Window.get().menu = appMenu;
```

# References

+ Getting started - [http://docs.nwjs.io/en/latest/For%20Users/Getting%20Started/#getting-started-with-nwjs](http://docs.nwjs.io/en/latest/For%20Users/Getting%20Started/#getting-started-with-nwjs)

+ Cutomizing menubar - [http://docs.nwjs.io/en/latest/For%20Users/Advanced/Customize%20Menubar/](http://docs.nwjs.io/en/latest/For%20Users/Advanced/Customize%20Menubar/)

+ Packaging - [https://github.com/nwjs-community/nw-builder](https://github.com/nwjs-community/nw-builder)

+ Packaging & distributing apps - [https://github.com/nwjs/nw.js/wiki/how-to-package-and-distribute-your-apps](https://github.com/nwjs/nw.js/wiki/how-to-package-and-distribute-your-apps)

+ Building NW.js - [http://docs.nwjs.io/en/latest/For%20Developers/Building%20NW.js/](http://docs.nwjs.io/en/latest/For%20Developers/Building%20NW.js/)

+ Windows build instructions - [https://chromium.googlesource.com/chromium/src/+/master/docs/windows_build_instructions.md](https://chromium.googlesource.com/chromium/src/+/master/docs/windows_build_instructions.md)

+ MAC build instructions - [https://chromium.googlesource.com/chromium/src/+/master/docs/mac_build_instructions.md](https://chromium.googlesource.com/chromium/src/+/master/docs/mac_build_instructions.md)

+ Linux build instructions - [https://chromium.googlesource.com/chromium/src/+/master/docs/linux_build_instructions.md](https://chromium.googlesource.com/chromium/src/+/master/docs/linux_build_instructions.md)

+ NW.js v.0.34.1 release with Node.js v11.0.0 update - [https://groups.google.com/forum/#!topic/nwjs-general/VCz6Zh_Doi8](https://groups.google.com/forum/#!topic/nwjs-general/VCz6Zh_Doi8)

+ Help (NW mailing list) - [https://groups.google.com/forum/#!forum/nwjs-general](https://groups.google.com/forum/#!forum/nwjs-general)

> ## Libraries

+ Context menu - [https://swisnl.github.io/jQuery-contextMenu//](https://swisnl.github.io/jQuery-contextMenu//)

+ Draggable - [https://jqueryui.com/draggable/](https://jqueryui.com/draggable/)

+ Droppable - [https://jqueryui.com/droppable/](https://jqueryui.com/droppable/)

+ Resizable - [https://jqueryui.com/resizable/](https://jqueryui.com/resizable/)

+ Selectable (Allows to select multiple elements) - [https://jqueryui.com/selectable/](https://jqueryui.com/selectable/)

+ Sortable (Allows to change the position of elements) - [https://jqueryui.com/sortable/](https://jqueryui.com/sortable/)

+ Tabs (jQuery UI tabs) - [https://jqueryui.com/tabs/](https://jqueryui.com/tabs/)

+ Email - nodemailer [https://thehackernews.com/2017/01/phpmailer-swiftmailer-zendmail.html](https://thehackernews.com/2017/01/phpmailer-swiftmailer-zendmail.html)

+ YouTube Data API v3 
  
  - https://developers.google.com/youtube/v3/quickstart/nodejs

  - https://console.developers.google.com/flows/enableapi?apiid=youtube&pli=1 (Wizard, google API console) => CANCEL

  - 