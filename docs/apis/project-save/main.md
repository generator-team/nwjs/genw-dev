# Project creation

```javascript
function collectProjectCreationDetails(event) {
    event.preventDefault();

    let projectName = document.getElementById("project-name").value;
    let path = document.getElementById('path').value;

    console.log(projectName);
    console.log(path);

    takeActionForProjectCreation(projectName, path);
}

function checkFileNameForExtension(fileName, extension = 'html') {
    /**
        + Check file name, if it is incorrect use 'index.html' as default one
        + If no extension is provided but file name is pure alphabetic, add 'html' as extension
    */
    try {
        if(!(/[a-zA-Z]+.html$/.test('' + fileName))) { // A12.html, index.html, web1992.html
            if(/^[a-zA-z][a-zA-Z0-9]+$/.test('' + fileName)) { // A, index, tiger67
                fileName = fileName + '.' + extension; // A.html, index.html, tiger67.html
            } else {
                fileName = 'index.' + extension; // index.html
            }
        }

        console.log('File name is correct: ' + `${fileName}`);
        return fileName; // index.html, about.html, prodcuts.html,
                         // contact.html, login.html, logout.html
    } catch(err) {
        console.log(`Error: ${err}`);
        return false; // Signal of an error
    }
}

function takeActionForProjectCreation(projectName, projectParentPath, fileName, ...args) {
    try {
        const fs = require('fs'); // File system
        const path = require('path'); // Paths 
        const copydir = require("copy-dir"); // To copy directories

        fs.accessSync(projectParentPath, fs.constants.F_OK); // Exists
        console.log('Path is okay');

        // Create project directory
        let projectPath = `${projectParentPath}${path.sep}${projectName}`; // Path to settings.json
        
        fs.mkdirSync(`${projectPath}${path.sep}.pypsi`, {
            recursive: true
        });

        console.log('Folder created, done');

        const d = new Date();
        let assetsSrc = path.resolve(`data${path.sep}assets`);
        let assetsDest = `${projectPath}${path.sep}assets`
        
        let settings = {
            projectName: projectName,
            projectPath: projectPath,
            projectRoot: `${projectParentPath}`,
            created: '' + d,
            shortCreated: d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear(),
            assets: assetsDest
        };

        copydir.sync(assetsSrc, assetsDest); // src, destination (Copying assets directory), path to assets copies all the folders

        fs.writeFileSync(`${projectPath}${path.sep}.pypsi${path.sep}settings.json`, JSON.stringify(settings, null, 4));

        return settings; // Will be helpful for UI developers
    } catch (err) {
        console.log(`Error (Project selection): ${err}`);
        alert(`Entered path does not exist in the system. ${err}`);

        return false; // Signal of an error
    }
}
```