https://api.linkedin.com/v1/people/~/shares?format=json

x-li-format: json
Content-Type: application/json
Authorization: Bearer <token>

raw
===

{
	"content": {
		"title": "How to be the honest?",
		"description": "Mediocrity is such an unfulfilling form of existence.",
		"submitted_url": "https://www.wikihow.com/Be-the-Best-You-Can-Be",
		"submitted_image_url": "https://www.wikihow.com/images/thumb/c/cc/Be-the-Best-Step-2-Version-2.jpg/aid1393314-v4-728px-Be-the-Best-Step-2-Version-2.jpg.webp"
	},
	"comment": "It is nice to see you here https://developer.linkedin.com/docs/oauth2",
	"visibility": {
    	"code": "anyone"
	}
}


