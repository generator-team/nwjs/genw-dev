PAGE 1 - 4
==========

+ Cross platform Desktop application (Windows, MAC, Linux {32/64 bit}) - Page 1a

+ Creating scientific reports and sharing them online - Page 1a

+ Kernels (Python, MATLAB, Octave, R) - Page 1a

+ Executing codes. Saving output (figures etc.) - Page 1a

+ CRON jobs, PDF reports 

+ TTS (Amazon Polly) - Page 1b

+ Modular design 
 
+ Word processor, MATHJAX, Text editor (3rd party printing press). - Page 2a

+ Auxiliary tools (Video manager, PDF manager, Spellcheck, Word count, Find replace, Thesaurus) - Page 2a

+------------------------+
| Options / UI : On hold |
+------------------------+

+ Online & Offline IDE - Page 2b

+  




