You can also try like this.

> re.I is for case insenistive match. You can check https://docs.python.org/3/library/re.html for more details.

	import re

	s = "valoirfinalieMONT:23maning"
	s2 = "montdj34meaing"
	s3 = "thisisthelastmontitwillwork98help"

	m = re.match(r".*(?P<name>mont)\D+(?P<number>\d+).*", s, re.I)
	print(m.group(1)) # MONT
	print(m.group(2)) # 23

	# Same as above (2nd way)
	print(m.group('name'));
	print(m.group('number'))

	m2 = re.match(r".*(?P<name>mont)\D+(?P<number>\d+).*", s2, re.I)
	print(m2.group(1)) # mont
	print(m2.group(2)) # 34

	m3 = re.match(r".*(?P<name>mont)\D+(?P<number>\d+).*", s3, re.I)
	print(m3.group(1)) # mont
	print(m3.group(2)) # 98
