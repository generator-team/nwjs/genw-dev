/*
	- 7 Jan 2018, Monday
	- http://www.sqlitetutorial.net/sqlite-nodejs/connect/

	- Connecting to disk file database
*/

const sqlite3 = require("sqlite3").verbose();
const md5 = require('md5');

// const query = `CREATE TABLE emails(
// id INTEGER PRIMARY KEY,
// to TEXT NOT NULL,
// cc TEXT DEFAULT '',
// bcc TEXT DEFAULT '',
// subject TEXT NOT NULL, 
// message TEXT DEFAULT '',
// attachments TEXT DEFAULT '')`;

// Using to/from as column names will throw error
const query = `CREATE TABLE IF NOT EXISTS EMAILS(
					id TEXT PRIMARY KEY NOT NULL,
					from_email TEXT NOT NULL,
					to_emails TEXT NOT NULL,
					cc TEXT DEFAULT '',
					bcc TEXT DEFAULT '',
					subject TEXT NOT NULL, 
					message TEXT DEFAULT '',
					attachments TEXT DEFAULT '',
					created_at TEXT,
					opened TEXT DEFAULT 'NO',
					active TEXT DEFAULT 'YES'
				)`;

// ( Very first ) Create Connection
let db = new sqlite3.Database('./db/pysi-proj.db', sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE
, (err) => {
	if (err) {
		console.error(err.message);
	} else {
		console.log('Connected to the chinook database.');
	}
});


/*
	> const md5 = require("md5")
	undefined
	>
	> md5(`${new Date()}`)
	'ac8f37762759a2eb0b947b2f6a3175aa'
	>
	> md5(`${new Date()}`)
	'bf506acab75b5ee60e228f1cb96238c9'
	> md5(`${new Date()}`)
	'cdf790d1b2a2bbb1ecaa0d704779164f'
	> md5(`${new Date()}`)
	'b7301d1a847e0496bbe576c111ab64f3'
	> md5(`${new Date()}`)
	'b7301d1a847e0496bbe576c111ab64f3'
	> md5(`${new Date()}`)
	'bee132cd27861e9168d27dcf6f53f1ce'
	> md5(`${new Date()}`)
	'3c38ecbdfdff20f161e45c4eb9a0e8b0'
	>
*/

// INSERT (If does not exist)
id = md5('' + new Date());
from = 'rishikesh2@gmail.com';
to = 'hemkesh2@gmail.com,py@gmail.com,node@gmail.com';
cc = 'cc2@gmail.com';
bcc = 'bcc2@gmail.com';
subject = `NW - documentation`;
message = `Enjoy NW!!!`;
attachments = `C:\\TC\\A.jpg;D:\\BG\\B.png`;

const query3 = `INSERT INTO EMAILS 
(
	id, from_email, to_emails, cc, 
	bcc, subject, message, attachments, created_at
)
VALUES	
(
	'${id}', '${from}', '${to}', '${cc}', '${bcc}',
	'${subject}', '${message}', '${attachments}',
	'${new Date()}'
)`;

const query4 = `SELECT * FROM EMAILS`;

// SELECT
//  datetime(d1,'unixepoch')
// FROM
//  datetime_int;

// EXEUTE QUERY
db.serialize(function() {
	// CREATE (If does not exist)
	db.run(query);
	console.log('Success');
	// console.log(md5('password@123'));

	db.run(query3);
	console.log('Data inserted');

	// db.run(query4);
	// console.log(`SELECTED THE INSERTED DATA`);

	db.all(query4, [], (err, rows) => {
		if(err) 
			throw err;

		console.log(rows);

		// rows.forEach((row) => {
		// 	console.log(row);
		// })

		// console.log('Done...');
	})
});


// ( Very last ) Close connection
db.close((err) => {
	if (err) {
		return console.error(err.message);
	}

	console.log(`Close the database connection.`);
});


