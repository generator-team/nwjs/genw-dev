I loved the question and though to write my answer by using the `reduce()` method defined on arrays.

function getIndices(text, delimiter='.') {
	let indices = [];
	let combined;

	text.split(delimiter)
		.slice(0, -1)
		.reduce((a, b) => { 
			if(a == '') {
				combined = a + b;
			} else { 
				combined = a + delimiter + b;
			} 

			indices.push(combined.length);
			return combined; // Uncommenting this will lead to syntactical errors
		}, '');

	return indices;
}


let indices = getIndices(`Ab+Cd+Pk+Djb+Nice+One`, '+');
let indices2 = getIndices(`Program.can.be.done.in.2.ways`); // Here default delimiter will be taken as `.`

console.log(indices);  // [ 2, 5, 8, 12, 17 ]
console.log(indices2); // [ 7, 11, 14, 19, 22, 24 ]

// To get output as expected (comma separated)
console.log(`${indices}`);  // 2,5,8,12,17
console.log(`${indices2}`); // 7,11,14,19,22,24