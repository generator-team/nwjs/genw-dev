# Root installation directory

```bash
C:\Program files\Generator\data\
								|
								Rishikesh (Username of system)
								|
								html
								|
								latex
								|
								social media
								|
								video
								|
								email
									|
									rishi@gmail.com
									|		|
									|		2018
									|		|  |
									|		|  +---Dec.json
									|		|
									|		2019
									|		|	|
									|		|	+---Jan.json
									|		|	|
									|		|	+---Feb.json
									|		|	|
									|		|	+---Mar.json
									|		|
									|		...	
									|		
									avi@gmail.com
									|		|
									|		2018
									|		|
									|		+---Dec.json
									|		|
									|		2019
									|		|
									|		+---Jan.json
									|		|
									|		+---Feb.json
									|		|
									|		+---Mar.json
									|
									...

```
