Computational module
====================

+ Currently we have tested Python (with PythonShell) & R like interpreted languages. And after this MATLAB / Octave will be next targets.

+ We're looking and trying more examples to work with it.

**Note:** There is no problem integrating other interpreted/scripting languages like perl, ruby etc.

Social media module
===================

+ For this, our first target is to finish it with Linkedin and YouTube (As we already done RnD before, so it is the time to integrate it with Desktop application and it is running). 

+ Then we will look for Facebook (As this needs extra time for review etc. once we created app on Facebook developers. And for that we should have something running in our website, like Facebook login, so it will be tackled later), Twitter etc. 

Your queries are always welcome.

Thank you.


