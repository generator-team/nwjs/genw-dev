Good evening sir, 

Today we have created 2 simple videos where you can have a look for the HTML generator, Email manager and Social media, how it starts, how will you choose the name of projects, how will you start another project etc. 

You will get links on your email, 1 after 1, 2 times, please check and download.

The 2nd video shows the back end work, file selection, directory structure listing, email sending, posting on Facebook and Linkedin, creating default structure of project. It's UI is poor as it is just for the purpose to test the Back end code and its functionality.

Currently, I have already tested Python's and R's execution on Back end with file based APIs. We will send it's video to you tomorrow.  

Currently, we have not integrated all so all are independent, once we are done with most of the things then we will start working on integration because that will also take time. 

So UI team is focusing on UI part. We are also looking for best free tools to integrate with the software, testing it and using it. Like emoji support while creating POST, CK editor, Theme integration for editing part, spelling check and many other things.

In video, you will not see most of the things as we have not started to work on LaTeX so we are not using LaTeX (few work is remaining), BibTeX (most of the part is done) APIs. That will be done just after LaTeX. 

Next target is YouTube in Social media part and then Facebook (Once we will start to work on Website). 

@Abhishek, please update on UI.

Thank you, very much.