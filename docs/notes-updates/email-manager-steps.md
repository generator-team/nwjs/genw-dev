STEPS
=====

1) To
2) From
3) CC
4) BCC
5) Subject
6) Bcc
7) Message
8) Select templates (Once template is selected, send capture the id of template)
9) Add files (image, txt, etc., should be in the limited size, approx. 20 MB)


HOW TO SEND DATA?
=================

> sendMail(from, to, cc, bcc, subject, message, files, templateId);

# DESCRIPTION ABOUT PARAMETERS

| Parameter | Description | Example | Type |
| ---  | --- | --- | --- |
| from | Google mail id    | ['rishi2@gmail.com'] | String |
| to   | List of mail ids  | ['rishi@sjainventures.com', 'hr@sjainventures.com', 'abc@gmail.com'] | Array |
| cc   | List of mail ids  | ['abc2@gmail.com', 'abc@gmail.com', 'pk@nic.in'] | Array |
| bcc  | List of mail ids  | ['abc3@gmail.com', 'mybuck@checkin.info'] | Array | 
| subject | Header, Brief  | Rishikesh | String | 
| message | Detailed description | Hello sir <br> Please check this | Text/String |
| files   | List of absolute paths of selected files | ['C:\\TC\\A.jpg', 'D:\\Files\\pdfs\\B.pdf', 'E:\\DS\\algo.txt'] | Array |
| templateId | Id of the selected template | 2 |int / dynamic string (md5 hash / base64 encoded of datetime) | 


> Note:-

+ Cheching the size of uploaded files.

+ Grabbing the absolute path uploaded files (use NW.js syntax or pure Js syntax).

+ Providing id 


