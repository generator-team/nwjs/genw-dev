# DIR STRUCUTRE:-
===============

C:\\Program Files\\
	|
	Generator\\data\\
			|
			email\\
				|
				admin@gmail.com\\
				|							
						[
							...
							...
							{
								'date': '12-jan-2018',
								...
								...
								...
							},
							{
								'date': '12-jan-2018',
								...
								...
								...
							},
							...
							...
						]
				|							
				|							
				|						
				|
				new.admin@gmail.com\\
		        |					[
		        |						...
		        |						...
				|						{
				|							'date': '12-jan-2018',
				|							...
				|							...
				|							...
				|						},
				|						{
				|							'date': '12-jan-2018',
				|							...
				|							...
				|							...
				|						}
				|						...
				|						...
				|					]
				|
				|
				...
				...
								
# MAIL CONTENTS
===============

1) Simple mail
2) Simple mail with attachments
3) HTML mail
4) HTML mail with attachments

+ Mail to single user
+ Mail to multiple users

#### ========================================================================


Hello everyone, today I heard a new thing while using **nodemailer** (https://nodemailer.com/about/) with gmail `SMTP`. It allows to send **500** emails daily but I am looking for no limitation. 

One of my colleague suggested to go for https://developers.sparkpost.com/ but here we need to do few more extra steps (Sign up => API key and limitation is for **1500** mails) unlike directly using **nodemailer**. 

Can anyone suggest any option (Node APIs) to send mail with no limitation. Pardon me to ask this as I know services are not here to use them freely for an unlimited amount of time in a day but I also think if there is anything as I want, please let me know.

Thank you team.