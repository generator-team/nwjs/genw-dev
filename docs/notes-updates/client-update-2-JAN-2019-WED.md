Good morning sir,

We want to inform you that we're working on Email manager. Currently we are using 
GMAIL SMTP server (where sender's mail will always be any valid gmail but recipients mail id can belong to other too) for the development.

Have a look at below lines:

'The Gmail mail server send limit is approximately 100-150 emails per day when connected to the server from a remote email client. You can send 500 emails each day through the Gmail SMTP server when logged into their web interface. The Comcast mail server limits residential customers to send 1,000 messages per day.'

Currently mail based Node API is ready and we are looking for a best way to manage these mails in the form of JSON (As saving data as JSON is much flexible than other like csv/xml/text) in user's system. 

Here is a sample JSON:

	Generator\Data\emails\2018\11\
								| 
								+ 1.json
								+ 2.json
								+ ...
								+ ...
								+ 30.json

And this is 30.json
===================

	{
	    "2018-11-30": {
	        "14:44:36": {
	            "from": "rishikesh0014051992@gmail.com",
	            "to": "rishikesh@sjainventures.com",
	            "cc": [],
	            "bcc": [],
	            "subject": "This is subject Kite",
	            "text": "Nice JOB",
	            "html": "<h2>Great!!!</h2>",
	            "attachments": [
	                {
	                    "filename": "Hacker.jpg",
	                    "path": "H:\\RishikeshAgrawani\\Projects\\Images\\Hacker.jpg",
	                    "cid": "abc-Doc-Project-1234"
	                },
	                {
	                    "filename": "README.md",
	                    "path": "H:\\RishikeshAgrawani\\Projects\\GenWork\\README.md",
	                    "cid": "abc#dddfDocE@!~$Project#12345"
	                },
	                {
	                    "filename": "Great-text-do-respect-girls.txt",
	                    "path": "H:\\RishikeshAgrawani\\Projects\\GenWork\\Great-text-do-respect-girls.txt",
	                    "cid": "abcDocProje*$#@ct#123"
	                },
	            ]
	        },
	        "14:44:59": {
	            "from": "rishikesh0014051992@gmail.com",
	            "to": "rishikesh@sjainventures.com",
	            "cc": [],
	            "bcc": [],
	            "subject": "This is subject Kite",
	            "text": "Hello dear",
	            "html": "<h1>Best one</h1>",
	            "attachments": [
	                {
	                    "filename": "Hacker.jpg",
	                    "path": "H:\\RishikeshAgrawani\\Projects\\Images\\Hacker.jpg",
	                    "cid": "abc-Doc-Project-1234"
	                },
	                {
	                    "filename": "README.md",
	                    "path": "H:\\RishikeshAgrawani\\Projects\\GenWork\\README.md",
	                    "cid": "abcDocP##?##roject#12345"
	                },
	                {
	                    "filename": "Great-text-do-respect-girls.txt",
	                    "path": "H:\\RishikeshAgrawani\\Projects\\GenWork\\Great-text-do-respect-girls.txt",
	                    "cid": "abcDocProject#123"
	                },
	            ]
	        }
	    }
	}

Note: In upgraded version, we can provide/allow user to see/open the sent mails.

There are other mail service providers like MailChimp, Sparkpost etc. we can go with them later for better support and getting rid of limitations.

Right now, 65% of work of email manager is done, now working on next 35%.