## Type your template here (2nd one will be modified by Backend team)

```html
<tr>
	<td class="checkTD"></td>
	<td>
		<div class="mail_list_content">
			<span class="mail_list_subject">Edinburgh</span>
			<span class="mail_list_separator">&nbsp;-&nbsp;</span>
			<span class="message">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
			</span>
		</div>
	</td>
	<td class="mail_date">
		<div class="dated_mail"><i class="fa fa-paperclip attach"></i>2011/01/25</div>
	</td>
</tr>
```

## 2nd one (This will be modified by Backend team)

```html
<tr>
	<td class="checkTD">chk-1</td>
	<td>
		<div class="mail_list_content">
			<span class="mail_list_subject">${email.subject}</span>
			<span class="mail_list_separator">&nbsp;-&nbsp;</span>
			<span class="message">
				${email.message}
			</span>
		</div>
	</td>
	<td class="mail_date">
		<div class="dated_mail"><i class="fa fa-paperclip attach"></i>${email.date}</div>
	</td>
</tr>
```