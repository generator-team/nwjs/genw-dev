// Entry point of the application
// https://github.com/nwjs/nw.js/issues/6308
nw.Window.open('./src/views/index.html', {
    title: "Generator",
	position: 'center',
	width: 1000,
	height: 600,
	frame: true
}, function(win) {
	// log the width / height = 1366 / 768
    if(typeof win !== 'undefined'){
        win.on('closed', function() {
            console.log('Generator app closed');
        });
        
        win.on('loaded', function() {
            console.log("Generator app started")
            win.focus();
        });
    }
});