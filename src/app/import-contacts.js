function importContacts(csvPath, ...args) {
	// var CsvReadableStream = require('csv-reader');
	// var AutoDetectDecoderStream = require('autodetect-decoder-stream');
	var fs = require('fs');

	// var inputStream = fs.createReadStream(csvPath)
	// 	.pipe(new AutoDetectDecoderStream({ defaultEncoding: '1255' })); // If failed to guess encoding, default to 1255

	// // The AutoDetectDecoderStream will know if the stream is UTF8, windows-1255, windows-1252 etc.
	// // It will pass a properly decoded data to the CsvReader.
	 
	// inputStream
	// .pipe(CsvReadableStream({ parseNumbers: true, parseBooleans: true, trim: true }))
	// .on('data', function (row) {
	//     console.log('A row arrived: ', row);
	// }).on('end', function (data) {
	//     console.log('No more rows!');
	// });

	// var loader = require('csv-load-sync');
	// var csv = loader(csvPath); // 'path/to/file.csv'
	// // csv is an Array of objects

	// function split(line, lineNumber) {
	//   if (lineNumber === 0) { // title line
	//     return line.split(',')
	//   }
	//   // our line will be <location>,<lat>,<lon>
	//   // and we want to combine lat and lon
	//   var parts = line.split(',')
	//   return [parts[0], parts[1] + ',' + parts[2]];
	// }

	// var results = loader(csvPath, {
	//   getColumns: split
	// });

	console.log(`${csvPath}`);

	let content = fs.readFileSync('.\\' + csvPath);
	let s = content.toString();
	let arr = [];

	s.split('\n').forEach((row) => {
		let cols = row.split(',')
		arr.push(cols.map((item => item.trim())));
	})
	console.log(arr);
}

importContacts('contacts.csv');
importContacts('contacts2.csv');

