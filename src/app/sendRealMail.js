const path = require('path');

function sendRealMail(from, to, subject, text, ...args) {
    // SEND MAIL (AS WE HAVE SUCCESSFULLY COLLECTED DATA FROM FORM)
    const nodemailer = require("nodemailer");
    const {userMail, userPass} = require("../app/secrets");

    console.log("Credentials", userMail, userPass);
    const transport = nodemailer.createTransport({
        service: "gmail",
        auth: {
            user: userMail,
            pass: userPass
        }
    });

    console.log("Transport: ", transport)

    /* -- For multiple receivers --

        {
            ...
            to: 'myfriend@yahoo.com, myotherfriend@yahoo.com',
            ...
        }
    */

    /*
        -- For HTML mail --

        {
            ...
            html: '<h1>Welcome</h1><p>That was easy!</p>',
            ...
        }
    */

    /* cc, bcc, attachments */

    /*
        ["rishikesh@sjainventures.com", "admin@gmail.com"]
        "rishikesh@sjainventures.com,admin@gmail.com"
    */

    const mailOptions = {
        from: userMail,
        to: to,
        cc: []
        bcc: []
        subject: subject,
        text: text,
        html: '<h1 style="color: green">Great one!!!</h1>'
        attachments: [{
            filename: 'Hacker.jpg',
            path: 'H:\\RishikeshAgrawani\\Projects\\Images',
            cid: 'abcDocProject#1234' //same cid value as in the html img src
        }]
    };

    console.log("Email options", mailOptions);

    try {
        transport.sendMail(mailOptions, (error, info) => {
            if(error) {
                console.log("ERROR: ");
                console.log(error);

                // --mixed-context => require()
                // window.location.href = "./error-mail.html"; // I predicted
            } else {
                console.log(Object.keys(info));
                console.log("Email sent: ", info.response);

                if(localStorage) {
                    localStorage.setItem('info', JSON.stringify(info))
                }

                /* "Create/Update" JSON file */
                if(!path.existsSync('data.json')) { // If file does not exist
                    fs.writeFile('data.json', JSON.stringify(mailOptions), (err) => {
                        console.log('Successful');

                        // --mixed-context => require()
                        window.location.href = "./success-mail.html"; // I predicted
                    })
                } else { // else1 starts
                    // Read the JSON file (if exists)
                    fs.readFile('data.json', (err, data) => {
                        if(err) {
                            console.log('ERROR (READ): ' + err);
                        } else {
                            data = JSON.loads(data.toString());
                            let date = new Date();

                            let date = d.getFullYear() + "-" + d.getMonth() + "-" + d.getDate();
                            let time = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();

                            if(data[date]) { // e.g. '2015-04-12' 
                                data[date][time] = mailOptions;
                            } else {
                                data[date] = {
                                    time: mailOptions
                                };
                            }

                            // Updating the JSON with new email data added
                            fs.writeFile('data.json', JSON.stringify(data), (err) => {
                                if(err) {
                                    console.log('ERROR (WRITE): ' + err);
                                } else {
                                    console.log('JSON successfully updated');
                                }
                            })
                        }
                    })
                } // else1 ends
            }
        });
    } catch (error) {
        console.log("ERROR:-", error)
        location.href = "error_mail.html";
    }
}