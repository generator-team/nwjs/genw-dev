import pandas as pd

l = [12, 45, 77, 22, 11];
print(l);

for n in l:
	print(n);


df = pd.DataFrame(l)
print(df);

df2 = pd.DataFrame({
	'fullname': ['A Z', 'B P', 'C E', 'D O', 'E P'],
	'age': [12, 3, 22, 11, 23]
})

print(df2)