/**
 * Created on: 27 Dec 2018, Tue
 * Coded by  : Rishikesh Agrawani 
 */

// // if notation (1st way) to import entities
// import PythonShell from 'python-shell';

// Direct way
// 1st way of doint things
const {PythonShell} = require("python-shell");

console.log('FIRST WAY =>\n');

PythonShell.run('loops.py', null, (err) => {
	// Handling error

	if(err) {
		console.log(`ERROR: ${err}`);
	} else {
		console.log(`SUCCESS`);
	}
});

// 2nd way of doing things
console.log("SECOND WAY =>\n");

let options = {
  mode: 'text',
  pythonPath: 'C:\\Users\\rishi\\Anaconda3\\python',
  pythonOptions: ['-u'], // get print results in real-time
  scriptPath: '.',
  args: ['value1', 'value2', 'value3']
};
 
PythonShell.run('loops.py', options, function (err, results) {
  if (err) throw err;
  // results is an array consisting of messages collected during execution
  console.log('results: %j', results);

  console.log(typeof results);
  console.log(Object.prototype.toString.call(results).slice(8, -1));
  console.log(results.join('\n'));
});