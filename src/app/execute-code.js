/**
 * Created on: 27 Dec 2018, Tue
 * Coded by  : Rishikesh Agrawani 
 */

// // If notation (1st way) to import entities
// import PythonShell from 'python-shell';



/*
    >
    > os.userInfo()
    { uid: -1,
      gid: -1,
      username: 'rishi',
      homedir: 'C:\\Users\\rishi',
      shell: null }
    >
    > os.userInfo().username
    'rishi'
    >
    > os.userInfo().homedir
    'C:\\Users\\rishi'
    >
*/

// 'C:\\Users\\rishi\\Anaconda3\\python'
// os.userInfo().homedir + path.sep + 'Anaconda3' + path.sep + `python`
// Python's path (Basically, this should be the type of PATH of )

const os = require("os")
const path = require('path');


function executeCode(
                      pythonFileName,
                      pythonPath = os.userInfo().homedir + path.sep + 'Anaconda3' + path.sep + `python`,                   
                      scriptPath = '.',
                      ...args
                    ) {

    // Direct way
    // 1st way of doint things
    const {PythonShell} = require("python-shell");

    // Starts here, goes further
    // console.log({ go: "C:\\Path\\A.js", py: "D:\\Users\\B.js" });

    let options = {
        mode: 'text',
        pythonPath: pythonPath, 
        pythonOptions: ["-u"],  // Get print results in real-time
        scriptPath: scriptPath,
        args: args
    };

    console.log(options);
    console.log(`Python file path: ${pythonPath}`);

    PythonShell.run(pythonFileName, options, function (err, results) {
        if (err) {
            // Results is an array consisting of messages collected during execution
            
            console.log(`Python => Error: ${err}`);
            console.log(`Changing innerText (Error) ${new Date()}`);

            try {
                document.getElementById('exec-error').innerText = `${err}`;
                document.getElementById('exec-out').innerText = '';
                console.log(`Finally done (Error)`);
            } catch(err) {
                alert(`${err}`)
            }
            // throw err;
        } else {
            // alert(`Great Python (Success)`);
            console.log('results: %j', results);
            console.log(typeof results);
            console.log(Object.prototype.toString.call(results).slice(8, -1));

            results = results.join('\n');
            console.log(results);
            console.log(`Changing innerText(Success) ${new Date()}`);
            try {
                document.getElementById('exec-out').innerText = results;
                alert('Changed');
                document.getElementById('exec-error').innerText = '';
                console.log('Finally Done')
            } catch(err) {
                alert('err')
            }
        }
    });
}

// executeCode('loops.py');